# PHP-Certification-Study

[![Build Status](https://travis-ci.org/ReenExe/PHP-Certification-Study.svg)](https://travis-ci.org/ReenExe/PHP-Certification-Study)

by book `Zend PHP 5.5 Certification Study Guide, Third Edition`
will be good to see: <https://github.com/php/php-langspec>


*    PHP Basics
*    Functions
*    Arrays
*    Object-oriented Programming
*    Security
*    Databases
*    Data Format & Types
*    Arrays
*    Strings & Patterns
*    Web Features
*    I/O

For use PHP 7 - https://github.com/rlerdorf/php7dev

### Construction
* `finally`
* `yield`
* `continue`
* `break`

### String
##### Functions
* `str_replace`
* `sscanf`
* `strpos`
* `strtr`
* `urlencode`
* `urldecode`
* `http_build_query`
##### Patterns
* `preg_match`
* `implode`
* `join` alias of `implode`
* `explode`

### OOP Magic
* `__call`
* `__set`
* `__isset`
* `__unset`
* `__get`
* `__call`
* `__callStatic`
* `__clone`
* `__sleep`
* `__wakeup`

### Arrays functions
##### Stack
* `array_push`
* `array_pop`
* `array_unshift`
* `array_shift`

##### Filter
* `array_filter`

##### Common
* `array_key_exists`
* `array_values`
* `array_keys`
* `in_array`
* `array_search`
* `array_merge`
* `array_merge_recursive`
* `array_flip`
* `array_map`
* `array_combine`
* `array_count_values`
* `array_slice`
* `array_chunk`
* `array_fill`
* `array_fill_keys`
* `array_column`
* `array_change_key_case`

##### Sets
* `array_unique`
* `array_diff`
* `array_intersect`
* `array_intersect_assoc`

##### Sort
* `sort`
* `asort`
* `arsort`
* `rsort`

### Type alias
* `int`
* `integer`

* `bool`
* `boolean`

* `float`
* `double`
* `real`
